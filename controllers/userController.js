const {user} = require('../models/mysql')
const passport = require('passport')
const jwt = require('jsonwebtoken')

class UserController{

  async signup(user, req,res) {
    const body = {
      id: user.dataValues.id,
      email:user.dataValues.email
    };

    const token = jwt.sign({
      user:body
    }, 'secret_password')

    res.status(200).json({
      message:'Signup success!',
      token:token
    })
  }

  async login(user, req,res) {
    const body = {
      id:user.dataValues.id,
      email:user.dataValues.email
    }

    const token = jwt.sign({
      user:body
    }, 'secret_password')

    res.status(200).json({
      message:'Login success!',
      token:token
    })

  }

}

module.exports = new UserController
