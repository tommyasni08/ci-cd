const {pelanggan} = require('../models/mongodb')

class PelangganController {

  async getAll(req,res) {
    pelanggan.find({}).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async getOne(req,res) {
    pelanggan.findOne({
      _id:req.params.id
    }).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async create(req,res) {
    pelanggan.create({
      "nama":req.body.nama
    }).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async update(req,res) {
    pelanggan.findOneAndUpdate({
      _id:req.params.id
    }, {
      "nama":req.body.nama
    }).then(()=>{
      return pelanggan.findOne({
        _id:req.params.id
      })
    }).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async delete(req,res){
    pelanggan.delete({
      _id:req.params.id
    }).then(r=>{
      res.json({
        status:'success',
        data:null
      })
    })
  }
}

module.exports =new PelangganController
