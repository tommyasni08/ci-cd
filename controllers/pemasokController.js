const {pemasok} = require('../models/mongodb')

class PemasokController {

  async getAll(req,res) {
    pemasok.find({}).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async getOne(req,res) {
    pemasok.findOne({
      _id:req.params.id
    }).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async create(req,res) {
    pemasok.create({
      "nama":req.body.nama
    }).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async update(req,res) {
    pemasok.findOneAndUpdate({
      _id:req.params.id
    }, {
      "nama":req.body.nama
    }).then(()=>{
      return pemasok.findOne({
        _id:req.params.id
      })
    }).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async delete(req,res){
    pemasok.delete({
      _id:req.params.id
    }).then(r=>{
      res.json({
        status:'success',
        data:null
      })
    })
  }
}

module.exports =new PemasokController
