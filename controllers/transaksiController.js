const {barang,pelanggan,transaksi} = require('../models/mongodb')

class TransaksiController {

  async getAll(req,res) {
    transaksi.find({}).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async getOne(req,res) {
    transaksi.findOne({
      _id:req.params.id
    }).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async create(req,res) {
    const data = await Promise.all([
      barang.findOne({
        _id:req.body.id_barang
      }),
      pelanggan.findOne({
        _id:req.body.id_pelanggan
      })
    ])
    let barangData = {
      "_id":data[0]._id,
      "nama":data[0].nama,
      "harga":data[0].harga,
      "pemasok":data[0].pemasok
    }
    let pelangganData = {
      "_id":data[1]._id,
      "nama":data[1].nama
    }
    let total = eval(data[0].harga.toString()) * req.body.jumlah
    transaksi.create({
      "barang": barangData,
      "pelanggan": pelangganData,
      "jumlah":eval(req.body.jumlah),
      "total":total
    }).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async update(req,res) {
    const data = await Promise.all([
      barang.findOne({
        _id:req.body.id_barang
      }),
      pelanggan.findOne({
        _id:req.body.id_pelanggan
      })
    ])
    let barangData = {
      "_id":data[0]._id,
      "nama":data[0].nama,
      "harga":data[0].harga,
      "pemasok":data[0].pemasok
    }
    let pelangganData = {
      "_id":data[1]._id,
      "nama":data[1].nama
    }
    let total = eval(data[0].harga.toString()) * req.body.jumlah

    transaksi.findOneAndUpdate({
      _id:req.params.id
    }, {
      "barang": barangData,
      "pelanggan": pelangganData,
      "jumlah": eval(req.body.jumlah),
      "total": total
    }).then(()=>{
      return transaksi.findOne({
        _id:req.params.id
      })
    }).then(r=>{
      res.json({
        status:'success',
        result:r
      })
    })
  }

  async delete(req,res){
    transaksi.delete({
      _id:req.params.id
    }).then(r=>{
      res.json({
        status:'success',
        data:null
      })
    })
  }
}

module.exports = new TransaksiController;
