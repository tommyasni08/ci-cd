const {barang,pemasok} = require('../models/mongodb');
const {ObjectId} = require('mongodb')

class BarangController {

  async getAll(req,res) {
    barang.find({}).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async getAllDeleted(req,res) {
    barang.findDeleted({}).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async getOne(req,res) {
    barang.findOne({
      _id:req.params.id
    }).then(r=>{
      res.json({
        status:'success',
        data:r
      })
    })
  }

  async create(req,res) {
    const data = await pemasok.findOne({
      _id:req.body.id_pemasok
    })
    let filteredData = {
      "_id":data._id,
      "nama":data.nama
    }
    return barang.create({
      "nama":req.body.nama,
      "harga":req.body.harga,
      "pemasok":filteredData,
      "image":req.file === undefined ? null : req.file.filename
    }).then(r=>{
      return res.json({
        "status":"success",
        "message":"barang added",
        "data":r
      })
    })
  }

  async update(req,res) {
    const data = await pemasok.findOne({
      _id:req.body.id_pemasok
    })
    let filteredData = {
      "_id":data._id,
      "nama":data.nama
    }
    barang.findOneAndUpdate({
      _id:req.params.id
    }, {
      "nama":req.body.nama,
      "harga":req.body.harga,
      "pemasok":filteredData,
      "image":req.file === undefined ? null : req.file.filename
    }).then(()=>{
      return barang.findById(req.params.id)
    }).then(r=>{
      res.json({
        status:'success',
        result:r
      })
    })
  }

  async delete(req,res) {
    barang.delete({
      _id:req.params.id
    }).then(r=>{
      res.json({
        status:'success',
        data:null
      })
    })
  }

  async restore(req,res) {
    barang.restore({
      _id:req.params.id
    }).then(()=>{
      return barang.findById(req.params.id)
    }).then(r=>{
      res.json({
        status:'success',
        result:r
      })
    })
  }
}



module.exports = new BarangController
