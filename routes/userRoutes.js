const express = require('express');
const router = express.Router();
const passport = require('passport');
const auth = require('../middlewares/auth')
const UserController = require('../controllers/userController')
const userValidator = require('../middlewares/validators/userValidator')

// router.post('/signup', [userValidator.signup, passport.authenticate('signup', {
//   session:false
// })], UserController.signup);
//
// router.post('/login', [userValidator.login, passport.authenticate('login', {
//   session:false
// })], UserController.login);

router.post('/signup' , [userValidator.signup, function(req,res,next) {
  passport.authenticate('signup', {
    session:false
  }, function(err,user,info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status:'Error',
        message: info.message
      });
      return;
    }
    UserController.signup(user,req,res,next);
  }) (req,res,next);
}]);

router.post('/login' , [userValidator.login, function(req,res,next) {
  passport.authenticate('login', {
    session:false
  }, function(err,user,info) {
    if (err) {
      return next(err);
    }
    if (!user) {
      res.status(401).json({
        status:'Error',
        message: info.message
      });
      return;
    }
    UserController.login(user,req,res);
  }) (req,res);
}]);

module.exports = router;
