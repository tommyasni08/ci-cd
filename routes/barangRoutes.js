const express = require('express')
const router = express.Router()
const passport = require('passport');
const auth = require('../middlewares/auth')
const BarangController = require('../controllers/barangController.js')
const barangValidator = require('../middlewares/validators/barangValidator.js')

router.get('/', [passport.authenticate('barang', {
  session:false
})], BarangController.getAll)

router.get('/delete', [passport.authenticate('barang', {
  session:false
})], BarangController.getAllDeleted)

router.get('/:id', [passport.authenticate('barang', {
  session:false
}), barangValidator.getOne], BarangController.getOne)

router.post('/create',[passport.authenticate('barang', {
  session:false
}), barangValidator.create], BarangController.create)

router.put('/update/:id', [passport.authenticate('barang', {
  session:false
}), barangValidator.update], BarangController.update)

router.put('/restore/:id', [passport.authenticate('barang', {
  session:false
}), barangValidator.restore], BarangController.restore)

router.delete('/delete/:id',[passport.authenticate('barang', {
  session:false
}), barangValidator.delete], BarangController.delete)

module.exports = router;
