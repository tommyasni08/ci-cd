# Setup packages/modules
npm install --save express express-validator mongoose mongoose-delete body-parser multer

npm init

"scripts": {"test" : "nodemon -r ./index.js"}

# NoSql database
## Create Database and Collection
1. Using Terminal
~$ mongo
~$ use (DBname)
~$ db.(collection).InsertMany([....])

2. Using GUI(Mongo Compass) Import files: Create JSON/CSV file, Database, collection and import JSON/CSV files

# Setup connection
Inside folder "models" create and code index.js file
Inside folder "models" create the schema files of each collection {barang,pemasok,pelanggan,transaksi}

# setup index.js file
import modules and routes.
Use bodyParser, express.static, routes, and listen to port.

# Validator files
Create and code validator files in "middlewares/validators" folder

# Controller files
Create and code controller files in controllers folder

# Route files
Create and code route files in routes folder

# Aunthentication
## Setup packages/modules
npm install --save bcrypt jsonwebtoken passport passport-jwt passport-local mysql2 sequelize

## Sequelize Setup
Create the .sequelizerc file // to differentiate between sequelize and mongodb files
sequelize init

configure the config.json file in config folder
sequelize db:create

sequelize model:generate --name user --attributes email:string,password:string,role:string // generate table user
Configure user.js file in "models/mysql" update the parameters like unique, custom setter/getter, etc.
edit user migration file: allowNull, unique, deletedAt
sequelize db:migrate


## Middlewares/aunthetication
inside the 'middlewares/auth' folder, create index.js
- passport "signup"
- passport "login"
- passport "(roles: transaksi, barang, pelanggan, pemasok)"

## userValidator
Create and code userValidator.js in 'middlewares/validators' folder

## userController
Create and code userController.js in controllers folder

## userRoutes
Create and code userRoutes.js in controllers folder


# Unit Testing using Mocha and Chai
## install global mocha
npm install -g mocha
or
sudo npm install -g mocha

## Install devPackage
npm install --save-dev mocha chai chai-http

## edit package.json
"scripts": {
  "test": "mocha ./tests/*.js --exit",
  "dev": "nodemon -r ./index.js"
}
 test 
## Use development (Postman)
npm run dev

##Use testing (Mocha)
npm test
