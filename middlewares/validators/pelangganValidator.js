const {pelanggan} = require('../../models/mongodb')
const {check,validationResult,matchedData,sanitize} = require('express-validator')

module.exports = {
  getOne:[
    check('id').custom(value=>{
      return pelanggan.findById(value).then(t=>{
        if (!t) {
          throw new Error('ID pelanggan tidak ada')
        }
      })
    }),
    (req,res,next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors:errors.mapped()
        })
      }
      next();
    }
  ],

  create:[
    check('nama').isAlpha().custom(value=> {
      return pelanggan.findOne({
        nama: value
      }).then(result => {
        if(result){
          throw new Error('Nama pelanggan sudah ada')
        }
      })
    }),
    (req,res,next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  update:[
    check('id').custom(value=>{
      return pelanggan.findById(value).then(p=>{
        if (!p) {
          throw new Error('ID pelanggan tidak ada')
        }
      })
    }),
    check('nama').isAlpha().custom(value=> {
      return pelanggan.findOne({
        nama: value
      }).then(result => {
        if(result){
          throw new Error('Nama pelanggan sudah ada')
        }
      })
    }),
    (req,res,next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],

  delete:[
    check('id').custom(value => {
      return pelanggan.findById(value).then(p => {
        if (!p) {
          throw new Error('ID pelanggan tidak ada!');
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ]

}
